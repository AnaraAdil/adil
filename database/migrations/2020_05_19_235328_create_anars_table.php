<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anars', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->mediumText('description')->nullable();
            $table->longText('content')->nullable();
            $table->mediumInteger('image_id')->nullable();
            $table->date('created_date')->nullable();
            $table->boolean('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anars');
    }
}
