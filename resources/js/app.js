/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';

window.Vue.use(VueRouter);

import AnarsIndex from './components/anars/AnarsIndex.vue';
import AnarsCreate from './components/anars/AnarsCreate.vue';
import AnarsEdit from './components/anars/AnarsEdit.vue';

const routes = [
    {
        path: '/',
        components: {
            anarsIndex: AnarsIndex
        }
    },
    {path: '/admin/anars/create', component: AnarsCreate, name: 'createAnar'},
    {path: '/admin/anars/edit/:id', component: AnarsEdit, name: 'editAnar'},
]

const router = new VueRouter({ routes })

const app = new Vue({ router }).$mount('#app')