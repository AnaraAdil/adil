<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anar extends Model
{
   protected $fillable = ['name', 'description', 'content', 'image_id', 'created_date', 'status'];
}
