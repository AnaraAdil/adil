<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anara extends Model
{
   protected $fillable = ['name', 'description', 'content', 'image_id', 'created_date', 'status'];
}
